<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LibrarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        \Illuminate\Support\Facades\DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        \Illuminate\Support\Facades\DB::table('library_models')->truncate();
        \Illuminate\Support\Facades\DB::table('library_models')->insert([
            [
                'authorid' => $faker->randomDigit(),
                'title' =>$faker->title,
                'ISBN' => $faker->countryCode,
                'pub_year'=>$faker->year,
                'avaiable'=>$faker->randomDigit()
            ],
            [
                'authorid' => $faker->randomDigit(),
                'title' =>$faker->title,
                'ISBN' => $faker->countryCode,
                'pub_year'=>$faker->year,
                'avaiable'=>$faker->randomDigit()
            ],
            [
                'authorid' => $faker->randomDigit(),
                'title' =>$faker->title,
                'ISBN' => $faker->countryCode,
                'pub_year'=>$faker->year,
                'avaiable'=>$faker->randomDigit()
            ],
            [
                'authorid' => $faker->randomDigit(),
                'title' =>$faker->title,
                'ISBN' => $faker->countryCode,
                'pub_year'=>$faker->year,
                'avaiable'=>$faker->randomDigit()
            ],
            [
                'authorid' => $faker->randomDigit(),
                'title' =>$faker->title,
                'ISBN' => $faker->countryCode,
                'pub_year'=>$faker->year,
                'avaiable'=>$faker->randomDigit()
            ],
            [
                'authorid' => $faker->randomDigit(),
                'title' =>$faker->title,
                'ISBN' => $faker->countryCode,
                'pub_year'=>$faker->year,
                'avaiable'=>$faker->randomDigit()
            ],

        ]);
        \Illuminate\Support\Facades\DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
