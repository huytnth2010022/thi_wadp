<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/table',[\App\Http\Controllers\LibraryController::class,'getlibrary']);
Route::get('/table/search',[\App\Http\Controllers\LibraryController::class,'search']);
