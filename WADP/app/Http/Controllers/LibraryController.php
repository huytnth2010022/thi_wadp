<?php

namespace App\Http\Controllers;

use App\Models\LibraryModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LibraryController extends Controller
{
    public function getlibrary(){
        $List = LibraryModel::paginate(5);
return view("table",["List"=>$List]);
    }
    public function search(Request $request){
$search = $request->search;
        $books = LibraryModel::find($search);
        return view("table",["List"=>$books]);

    }
}
