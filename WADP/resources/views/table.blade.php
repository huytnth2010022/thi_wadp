<!DOCTYPE html>
<html>
<head>
<title>Table layout</title>
<link rel="stylesheet" href="/asset/table.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <div class="search">
        <form action="/table/search" method="get">
            @csrf
        <input name="search" type="number">
        <button>Search</button>
        </form>
    </div>
<div class="filter">

<table >
<tr>
<th>#</th>
<th>Author ID</th>
<th>Title</th>
<th>ISBN</th>
<th>Year</th>
<th> Avaiable</th>

</tr>

@foreach ($List as $ap)
<tr>
    <td>{{$ap->bookid }}</td>
    <td>{{$ap->authorid}}</td>
    <td>{{$ap->title}}</td>
    <td>{{$ap->ISBN}}</td>
    <td>{{$ap->pub_year}}</td>
    <td>{{$ap->avaiable}}</td>
    
    </tr>

@endforeach

</table>
</div>
<div class="footer">


@if ($List->lastPage() > 1)
<ul class="pagination">
    <li class=""page-item díabled {{ ($List->currentPage() == 1) ? ' disabled' : '' }}">
        <a class="page-link"href="{{$List->url(1) }}">Previous</a>
    </li>
    @for ($i = 1; $i <= $List->lastPage(); $i++)
        <li class="page-item {{ ($List->currentPage() == $i) ? ' active' : '' }}">
            <a class="page-link" href="{{ $List->url($i) }}">{{ $i }}</a>
        </li>
    @endfor
    <li class="page-item díabled {{ ($List->currentPage() == $List->lastPage()) ? ' disabled' : '' }}">
        <a class="page-link" href="{{ $List->url($List->currentPage()+1) }}" >Next</a>
    </li>
</ul>
@endif
</div>
</body>
</html>
